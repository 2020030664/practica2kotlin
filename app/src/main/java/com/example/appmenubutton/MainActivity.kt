package com.example.appmenubutton

import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView

class MainActivity : AppCompatActivity() {

    private lateinit var buttonNavigationView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()

        setContentView(R.layout.activity_main)
        buttonNavigationView = findViewById(R.id.btnNavegador)
        buttonNavigationView.setOnItemSelectedListener {
                menuItem ->
            when(menuItem.itemId){
                R.id.btnHome -> {
                    cambiarFrame(HomeFragment())
                    true
                }
                R.id.btnLista -> {
                    cambiarFrame(ListFragment())
                    true
                }
                R.id.btnDb -> {
                    cambiarFrame(dbFragment())
                    true
                }
                R.id.btnSalir -> {
                    cambiarFrame(SalirFragment())
                    true
                }
                else -> false
            }
        }

    }

    private fun cambiarFrame(fragment: Fragment){

        // LINEA PARA CAMBIAR DE FRAMA
        supportFragmentManager.beginTransaction().replace(R.id.frmContenedor,fragment).commit()
    }
}